//#include "uart_stm32f1.h"
//#include "stm32f4xx.h"  


//GPIO 4 SPI CS
//#define GPIOA4_init                        	    \
//{                                           	\
//	.MODER = PWM_CLOCKSOURCE_CLK_IO,			\
//	.OTYPER = PWM123_CLOCKPRESCALEDBY_256,	\
//	.OSPEEDR = WAVEMODE_EDGE_ALIGN,			\
//	.PUPDR =  WAVEOUT_PPULSE,					\
//	.IDR =  WAVEOUT_PPULSE,					\
//	.ODR =  WAVEOUT_PPULSE,					\
//	.BSRR = ENABLE,             		  		\
//	.LCKR = DISABLE,							\
//  .AFR[0] = OUTPUT,						    \
//	.AFR[1] = OUTPUT,						    \
//}

////GPIO 5 SPI SCLK
//#define GPIOA5_init                        	    \
//{                                           	\
//	.MODER = PWM_CLOCKSOURCE_CLK_IO,			\
//	.OTYPER = PWM123_CLOCKPRESCALEDBY_256,	\
//	.OSPEEDR = WAVEMODE_EDGE_ALIGN,			\
//	.PUPDR =  WAVEOUT_PPULSE,					\
//	.IDR =  WAVEOUT_PPULSE,					\
//	.ODR =  WAVEOUT_PPULSE,					\
//	.BSRR = ENABLE,             		  		\
//	.LCKR = DISABLE,							\
//  .AFR[2] = OUTPUT,						    \
//}
////GPIO 6 SPI DO
//#define GPIOA4_init                        	    \
//{                                           	\
//	.MODER = PWM_CLOCKSOURCE_CLK_IO,			\
//	.OTYPER = PWM123_CLOCKPRESCALEDBY_256,	\
//	.OSPEEDR = WAVEMODE_EDGE_ALIGN,			\
//	.PUPDR =  WAVEOUT_PPULSE,					\
//	.IDR =  WAVEOUT_PPULSE,					\
//	.ODR =  WAVEOUT_PPULSE,					\
//	.BSRR = ENABLE,             		  		\
//	.LCKR = DISABLE,							\
//  .AFR[2] = OUTPUT,						    \
//}
////GPIO 7 SPI DI
//#define GPIOA4_init                        	    \
//{                                           	\
//	.MODER = PWM_CLOCKSOURCE_CLK_IO,			\
//	.OTYPER = PWM123_CLOCKPRESCALEDBY_256,	\
//	.OSPEEDR = WAVEMODE_EDGE_ALIGN,			\
//	.PUPDR =  WAVEOUT_PPULSE,					\
//	.IDR =  WAVEOUT_PPULSE,					\
//	.ODR =  WAVEOUT_PPULSE,					\
//	.BSRR = ENABLE,             		  		\
//	.LCKR = DISABLE,							\
//  .AFR[2] = OUTPUT,						    \
//}
////GPIO 9 usart Tx
//#define GPIOA4_init                        	    \
//{                                           	\
//	.MODER = PWM_CLOCKSOURCE_CLK_IO,			\
//	.OTYPER = PWM123_CLOCKPRESCALEDBY_256,	\
//	.OSPEEDR = WAVEMODE_EDGE_ALIGN,			\
//	.PUPDR =  WAVEOUT_PPULSE,					\
//	.IDR =  WAVEOUT_PPULSE,					\
//	.ODR =  WAVEOUT_PPULSE,					\
//	.BSRR = ENABLE,             		  		\
//	.LCKR = DISABLE,							\
//  .AFR[2] = OUTPUT,						    \
//}
////GPIO 10 usart Rx
//#define GPIOA4_init                        	    \
//{                                           	\
//	.MODER = PWM_CLOCKSOURCE_CLK_IO,			\
//	.OTYPER = PWM123_CLOCKPRESCALEDBY_256,	\
//	.OSPEEDR = WAVEMODE_EDGE_ALIGN,			\
//	.PUPDR =  WAVEOUT_PPULSE,					\
//	.IDR =  WAVEOUT_PPULSE,					\
//	.ODR =  WAVEOUT_PPULSE,					\
//	.BSRR = ENABLE,             		  		\
//	.LCKR = DISABLE,							\
//  .AFR[2] = OUTPUT,						    \
//}